# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from devpers import explore

class Os:
    """
    Embed OS peripheral class.
    """
    def __init__(self):
        self.pert = 3
        self.perte = 3
        self.par1 = 22
        self.par2 = 131
        self.dpaver = b'\x13\x04'
        self.security = [0] * 16

    def ReadOs(self, network, addr):
        """
        Creates a response containing information about the device such as module id, version of OS, flags, bonding key and implemented peripherals.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of device to read information from.
        :return: Error number, response data containing device information and network.
        """
        perenum = explore.Enumerate(network, addr)[1]
        ret = []
        ret.extend(network[str(addr)].mid)
        ret.extend(b'\x43\xb4\xc8\x08\x00\x31\x20\x75')
        ret.extend(network[str(addr)].ibk)
        ret.extend(perenum)
        return 0, ret, network

    def Reset(self, network, addr):
        """
        Performs a device reset. This clears RAM, resets the state of LEDs and closes the UART peripheral interface.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of device to reset.
        :return: Error number, empty response data and network.
        """
        if str(5) in network[str(addr)].embedpers:
            network[str(addr)].embedpers[str(5)].memory = [0] * 48
        if str(6) in network[str(addr)].embedpers:
            network[str(addr)].embedpers[str(6)].state = 0
        if str(7) in network[str(addr)].embedpers:
            network[str(addr)].embedpers[str(7)].state = 0
        if str(12) in network[str(addr)].embedpers:
            network[str(addr)].embedpers[str(12)].baudrate = -1
            network[str(addr)].embedpers[str(12)].recbuffer = [-1] * 64
            network[str(addr)].embedpers[str(12)].transbuffer = [-1] * 64
        return 0, [], network

    def Restart(self, network, addr):
        """
        Performs a device restart. Unlike reset, this only affects the RAM.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of device to restart.
        :return: Error number, empty response data and network.
        """
        if str(5) in network[str(addr)].embedpers:
            network[str(addr)].embedpers[str(5)].memory = [0] * 48
        return 0, [], network

    def SetSecurity(self, network, type, data):
        """
        This function writes security data.
        :param network: dictionary, represent the emulated network
        :param type: int, only value 0 and 1 are correct;y
        :param data: list, list of data
        """
        if type == 0 or type == 1:
            self.security = data
            return 0, [], network
        else:
            return 6, [], network

    def Sleep(self, network, addr):
        """
        This function performs device sleep. This clears SPI and close UART peripheral
        :param network: dictionary, represent the emulated network
        :param addr: int, address of device in the network
        """
        if addr == 0:
            return 3, [], network
        else:
            if str(8) in network[str(addr)].embedpers:
                network[str(addr)].embedpers[str(8)].rx = [-1] * 55
                network[str(addr)].embedpers[str(8)].tx = [-1] * 55
            if str(12) in network[str(addr)].embedpers:
                network[str(addr)].embedpers[str(12)].baudrate = -1
                network[str(addr)].embedpers[str(12)].recbuffer = [-1] * 64
                network[str(addr)].embedpers[str(12)].transbuffer = [-1] * 64
        return 0, [], network


    def ReadCfg(self, network, addr):
        """
        This function reads configuration data.
        Data consist of enumerate information, configuration, rfpgm and undocmuneted value.
        :param network: dictionary, represent the emulated network
        :param addr: int, address of device in the network
        """
        perenum = explore.Enumerate(network, addr)[1]
        ret = []
        a = 95
        ret.extend(perenum[2:6])
        ret.extend(network[str(addr)].configuration)
        ret = ret + [network[str(addr)].rfpgm]
        ret = ret + [network[str(addr)].undocumented]

        for i in range(0, len(ret)):
            a ^= ret[i]

        ret = [a] + ret
        return 0, ret, network

    def WriteCfg(self, network, addr, configuration, rfpgm):
        """
        This function writes configuration data.
        :param network: dictionary, represent the emulated network
        :param addr: int, address of device in the network
        :param configuration: data of configuration
        :param rfpgm: int, data of rfpgm
        """
        network[str(addr)].configuration = configuration
        network[str(addr)].rfpgm = rfpgm
        self.Sleep(network, addr)
        return 0, [], network


    def FactorySettings(self, network, addr):
        """
        Sets a device to it's factory settings state. This means that some bytes of the TR configuration are set to default value and the device is unbonded.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of the device to execute factory settings on.
        :return: Error number, empty response data and network.
        """
        if addr != 0:
            network[str(addr)].configuration[3] = 7
            network[str(addr)].configuration[4] = 5
            network[str(addr)].embedpers["1"].bonded = 0
            return 0, [], network
        else:
            return 3, [], network

    def Indicate(self, network, addr, control):
        """
        Controls the device indication. In this case, it is visual indication using the LED peripherals.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of the device to execute indication on.
        :param control: State of the LEDs.
        :return: Erro number, empty response data and network.
        """
        if str(6) in network[str(addr)].embedpers:
            if control in [0, 2, 3]:
                network[str(addr)].embedpers[str(6)].state = 0
            elif control == 1:
                network[str(addr)].embedpers[str(6)].state = 1
        if str(7) in network[str(addr)].embedpers:
            if control in [0, 2, 3]:
                network[str(addr)].embedpers[str(7)].state = 0
            elif control == 1:
                network[str(addr)].embedpers[str(7)].state = 1
        return 0, [], network


    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, network, addr, cmd, data):
        """
        Request handler for the OS peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param network: Dictionary representing the emulated network.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            return self.ReadOs(network, addr)
        elif cmd == 1:
            return self.Reset(network, addr)
        elif cmd == 2:
            return self.ReadCfg(network, addr)
        elif cmd == 3:
            # RFPGM - programming mode request.
            return 0, [], network
        elif cmd == 4:
            return self.Sleep(network, addr)
        elif cmd == 6:
            return self.SetSecurity(network, data[0], data[1:16])
        elif cmd == 7:
            if addr == 0:
                return 3, [], network
            else:
                return self.Indicate(network, addr, data[0])
        elif cmd == 8:
            return self.Restart(network, addr)
        elif cmd == 12:
            # Test RF Signal request, there isn't a great way to implement this functionality so the default and most common value is returned.
            if addr == 0:
                return 0, [1], network
            else:
                return 3, [], network
        elif cmd == 13:
            return self.FactorySettings(network, addr)
        elif cmd == 15:
            return self.WriteCfg(network, addr, data[5:32], data[32])
        elif cmd == 63:
            return 0, self.GetPerInfo(), network
        else:
            return 2, [], network
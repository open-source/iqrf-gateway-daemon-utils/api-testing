# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Thermometer:
    """
    Embed Thermometer peripheral class.
    :param temp: Temperature of the emulated thermometer peripheral.
    """
    def __init__(self, temp):
        self.temp = temp
        self.pert = 11
        self.perte = 1
        self.par1 = 0
        self.par2 = 0

    def Read(self):
        """
        Reads temperature from the emulated thermometer peripheral.
        :return: Error number and response data containing temperature.
        """
        if self.temp >= 0:
            temph = int(self.temp * 16)
            return 0, [int(self.temp), (temph & 0xff), (temph >> 8)]
        else:
            temph = int((256 + self.temp) * 16)
            return 0, [int(256+self.temp), (temph & 0xff), (temph >> 8)]

    def GetPerInfo(self):
        """
        Returns an array containing peripheral specific properties for peripheral information request.
        """
        return [self.perte, self.pert, self.par1, self.par2]

    def HandleRequest(self, cmd):
        """
        Request handler for the thermometer peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            return self.Read()
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Ledr:
    """
    Embed LEDR peripheral class.
    """
    def __init__(self):
        self.pert = 7
        self.perte = 3
        self.par1 = 0
        self.par2 = 0
        self.state = 0

    def Set(self, state):
        """
        Sets the LED to ON or OFF state depending on the request value.
        :param state: State to set the LED to.
        :return: Error number and empty response data.
        """
        self.state = state
        return 0, []

    def Pulse(self):
        """
        Sets the LED to pulse state, meaning it flashes once.
        :return: Error number and empty response data.
        """
        return 0, []

    def Flash(self):
        """
        Sets the LED to flashing state.
        :return: Error number and empty response data.
        """
        self.state = 2
        return 0, []

    def GetPerInfo(self):
        """
        Returns an array containing peripheral specific properties for peripheral information request.
        """
        return [self.perte, self.pert, self.par1, self.par2]

    def HandleRequest(self, cmd):
        """
        Request handler for the LEDR peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0 or cmd == 1:
            return self.Set(cmd)
        elif cmd == 3:
            return self.Pulse()
        elif cmd == 4:
            return self.Flash()
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
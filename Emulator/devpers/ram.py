# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Ram:
    def __init__(self):
        self.memory = [0] * 48
        self.pert = 6
        self.perte = 3
        self.par1 = 48
        self.par2 = 48

    def Read(self, address, length):
        """
        The funcion reads the data of deremination of the length from the specified address
        :param address: int, address in the memor
        :param length: int, lenght of data
        """
        if address >= 48:
            return 4, []
        elif address + length > 48:
            return 4, []
        else:
            return 0, self.memory[address:address+length]

    def Write(self, address, data):
        """
        The funcion writes the data form a specific position
        :param address: int, address in the memory
        :param data: list, data that will be written
        """
        if address >= 48:
            return 4, []
        elif address + len(data) > 48:
            return 4, []
        else:
            i = 0
            for j in range(address, address + len(data)):
                self.memory[j] = data[i]
                i += 1
        return 0, []


    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        """
        Handler for the node peripheral.
        :param cmd: int, peripheral command number
        :param data: list, optional command data
        """
        if cmd == 0:
            return self.Read(data[0], data[1])
        elif cmd == 1:
            return self.Write(data[0], data[1:len(data)])
        if cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Node:
    def __init__(self, bonded):
        self.bonded = bonded
        self.pert = 2
        self.perte = 3
        self.par1 = 56
        self.par2 = 0
        self.network_data = [16, 139, 220, 162, 99, 237, 240, 96, 17, 35, 77, 205, 28, 174, 177, 39, 180, 131, 226, 181,
                             23, 1, 21, 176, 209, 24, 61, 237, 237, 106, 169, 130, 145, 21, 228, 142, 203, 169, 223, 62,
                             157, 52, 134, 86, 123, 143, 197, 76, 6]


    def RemoveBond(self, network, addr):
        """
        The function set at the addr on the network value bonded to zero
        :param network: dictionary, represent the emulated network
        :param addr: int, address of device in the network
        """
        if network[str(addr)].embedpers[str(1)].bonded == 1:
            network[str(addr)].embedpers[str(1)].bonded = 0
            return 0, [], network
        else:
            return 4, [], network

    def BackupN(self, network, index):
        """
        The function read node network data.
        :param network: dictionary, represent the emulated network
        :param index: int, index in the data
        """
        ret = []
        if index > 49:
            return 6, ret, network
        else:
            for i in range(index, len(self.network_data)):
                ret.append(self.network_data[i])

        return 0, ret, network

    def RestoreN(self, network, data):
        """
        The function write new network data to node.
        :param network: dictionary, represent the emulated network
        :param data: list, data that will be written
        """
        if len(data) != 49:
            return 6, [], network

        self.network_data = []
        for i in range(0, len(data)):
            self.network_data.append(data[i])

        return 0, [], network

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, network, addr, cmd, data):
        """
        Handler for the node peripheral.
        :param network: dictionary, represent the emulated network
        :param addr: int, address of device in the network
        :param cmd: int, peripheral command number
        :param data: list, optional command data
        """

        if cmd == 1:
            return self.RemoveBond(network, addr)
        elif cmd == 6:
            return self.BackupN(network, data[0])
        elif cmd == 7:
            return self.RestoreN(network, data[0:len(data)])
        elif cmd == 8:
            return 0, [], network
        elif cmd == 63:
            return 0, self.GetPerInfo(), network
        else:
            return 2, [], network

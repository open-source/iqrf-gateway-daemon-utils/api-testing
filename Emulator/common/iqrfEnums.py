# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import enum

class IqrfPnum(enum.IntEnum):
    """
    Enumeration class containing peripheral types with their respective numbers.
    """
    Coordinator = 0
    Node = 1
    OS = 2
    EEPROM = 3
    EEEPROM = 4
    RAM = 5
    LEDR = 6
    LEDG = 7
    SPI = 8
    IO = 9
    Thermometer = 10
    UART = 12
    FRC = 13
    DALI = 74
    BO = 75
    Sensor = 94
    Light = 113
    Explore = 255

class IqrfSType(enum.IntEnum):
    """
    Enumeration class containing types of sensors for the Sensor standard with their respective numbers.
    """
    Temp = 1
    CO2 = 2
    VOC = 3
    ELVoltage = 4
    EMG = 5
    LVoltage = 6
    Current = 7
    Power = 8
    MFreq = 9
    TimeSpan = 10
    Lux = 11
    NO2 = 12
    SO2 = 13
    CO = 14
    O3 = 15
    APressure = 16
    CTemp = 17
    PPM25 = 18
    SPL = 19
    Alt = 20
    Acc = 21
    NH3 = 22
    Methane = 23
    ShortL = 24
    PPM1 = 25
    PPM4 = 26
    PPM10 = 27
    RHumidity = 128
    Bin7 = 129
    PowerFactor = 130
    UVIndex = 131
    pH = 132
    Bin30 = 160
    Consumption = 161
    Datetime = 162
    TimeSpanLong = 163
    Lat = 164
    Lon = 165
    Data = 192
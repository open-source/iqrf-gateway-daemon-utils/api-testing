# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Eeeprom:
    """
    Embed EEEPROM peripheral class.
    """
    def __init__(self):
        self.memory = [0] * 32768
        self.pert = 5
        self.perte = 3
        self.par1 = 128
        self.par2 = 0

    def ERead(self, address, length):
        """
        Reads bytes from EEEPROM memory and returns them.
        :param address: EEEPROM address to read from.
        :param length: Number of bytes to read from the address.
        :return: Error number, empty response data if the data read is longer than the DPA response can fit.
        """
        ret = []
        if length > 54:
            return 5, ret
        else:
            address = int(format(address[1], '08b')+format(address[0],'08b'), 2)
            # DPA currently considers address out of range overflow and starts from 0 + overflow
            if address > 32767:
                address -= 32768
            if address + length > 32767:
                overflow = address + length - 32768
                ret = self.memory[address:32767]
                ret.extend(self.memory[0:overflow])
            else:
                ret = self.memory[address:(address + length)]
            return 0, ret

    def EWrite(self, address, data):
        """
        Writes data into the EEEPROM memory to a specified address.
        :param address: EEEPROM address specifying where to start write from.
        :param data: Array of integers containing data to be written to the EEEPROM memory.
        :return: Error number, empty response data.
        """
        address = int(format(address[1], '08b')+format(address[0],'08b'), 2)
        if address >= 16383:
            return 4, []
        else:
            i = 0
            for j in range(address, address + len(data)):
                self.memory[j] = data[i]
                i += 1
            return 0, []

    def GetPerInfo(self):
        """
        Returns an array containing peripheral specific properties for peripheral information request.
        """
        return [self.perte, self.pert, self.par1, self.par2]

    def HandleRequest(self, cmd, data):
        """
        Request handler for the EEEPROM peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 2:
            if len(data) != 3:
                return 5, []
            return self.ERead(data[0:2], data[2])
        elif cmd == 3:
            return self.EWrite(data[0:2], data[2:len(data)])
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
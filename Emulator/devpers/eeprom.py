# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Eeprom:
    def __init__(self):
        self.pert = 4
        self.perte = 3
        self.par1 = 192
        self.par2 = 55
        self.memory = [0] * 192

    def Read(self, nadr, address, length):
        """
        The funcion reads the data of deremination of the length from the specified address
        :param nadr: int, address of device
        :param address: int, address in the memory
        :param length: int, lenght of data
        """
        if nadr == 0:
            if 128 <= address < 192:
                if address + length >= 192:
                    return 4, []
                return 0, self.memory[address:address+length]
            else:
                return 4, []

        else:
            if address >= 192:
                return 4, []
            elif address + length > len(self.memory):
                return 4, []
            else:
                return 0, self.memory[address:address+length]

    def Write(self, nadr, address, data):
        """
        The funcion writes the data form a specific position
        :param nadr: int, address of device
        :param address: int, address in the memory
        :param data: list, data that will be written
        """
        if nadr == 0:
            if 128 <= address < 192:
                if address + len(data) >= 192:
                    return 4, []
                i = 0
                for j in range(address, address + len(data)):
                    self.memory[j] = data[i]
                    i += 1
            else:
                return 4, []
        else:
            if address >= 192:
                return 4, []
            elif address + len(data) > 192:
                return 4, []
            else:
                i = 0
                for j in range(address, address + len(data)):
                    self.memory[j] = data[i]
                    i += 1
            return 0, []

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, nadr, cmd, data):
        """
        Handler for the eeprom peripheral.
        :param nadr: int, address of device
        :param cmd: list, optional command data
        :param data: list, optional command data
        """
        if cmd == 0:
            return self.Read(nadr, data[0], data[1])
        elif cmd == 1:
            return self.Write(nadr, data[0], data[1:len(data)])
        if cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Coordinator:
    """
    Embed coordinator peripheral class.
    :param did: Discovery ID of the network.
    """
    def __init__(self, did):
        self.did = did
        self.discovered = [0] * 256
        self.bonded = [0] * 256
        self.mids = dict()
        self.rqhops = 255
        self.rsphops = 255
        self.pert = 1
        self.perte = 3
        self.par1 = 56
        self.par2 = 0
        self.network_data = [16, 121, 146, 138, 36, 179, 60, 4, 230, 44, 153, 177, 197, 61, 87, 210, 180, 131, 226, 181,
                             23, 1, 21, 176, 209, 24, 61, 237, 237, 106, 169, 130, 145, 21, 228, 142, 203, 169, 223, 62,
                             157, 52, 134, 86, 123, 143, 197, 76, 168]

    def countBonded(self):
        """
        Checks internal memory of the coordinator device and returns number of bonded devices.
        :return count: Number of bonded devices.
        """
        count = 0
        for i in range(len(self.bonded)):
            if self.bonded[i] == 1:
                count += 1
        return count

    def AddrInfo(self, network):
        """
        This function returns info about address. This info contains of number of bonded devices and did value.
        :param network: dictionary, represent the emulated network
        """
        return 0, [self.countBonded(), self.did], network

    def DiscoveredNodes(self, network):
        """
        Checks internal memory of the coordinator device and returns array of integers representing bitmask of discovered nodes
        :param network: Dictionary representing the emulated network.
        :return: Error number, bitmask of discovered nodes and network.
        """
        ret = []
        for i in range(32):
            tmp = self.discovered[i*8:(i*8+8)][::-1]
            tmp = ''.join(str(j) for j in tmp)
            ret.append(int(tmp, 2))
        return 0, ret, network

    def BondedNodes(self, network):
        """
        Checks internal memory of the coordinator device and returns array of integers representing bitmask of bonded nodes
        :param network: Dictionary representing the emulated network.
        :return: Error number, bitmask of bonded nodes and network.
        """
        ret = []
        for i in range(32):
            tmp = self.bonded[i*8:(i*8+8)][::-1]
            tmp = ''.join(str(j) for j in tmp)
            ret.append(int(tmp, 2))
        return 0, ret, network

    def ClearBonds(self, network):
        """
        Clears all bonds between coordinator and nodes in the internal memory of the coordinator, and clears the discovered nodes memory.
        :param network: Dictionary representing the emulated network.
        :return: Error number, empty response data and network.
        """
        self.bonded = [0] * 256
        self.discovered = [0] * 256
        return 0, [], network

    def BondNode(self, network, addr):
        """
        Creates a bond between the coordinator device and a node device in the internal memory of the coordinator. Node configuration is changed
        to signify that it is a part of a network. If the address is 0, the first free address is used.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of node device to bond.
        :return: Error number, response data and network.
        """
        if addr == 0:
            for i in range(1, 240):
                if self.bonded[i] == 0 and str(i) in network:
                    self.bonded[i] = 1
                    network[str(i)].embedpers["1"].bonded = 1
                    return 0, [i, self.countBonded()], network
            return 1, [], network
        elif addr >= 240:
            return 1, [], network
        else:
            if self.bonded[addr] == 1:
                return 1, [], network
            else:
                if str(addr) in network:
                    self.bonded[addr] = 1
                    network[str(addr)].embedpers["1"].bonded = 1
                    return 0, [addr, self.countBonded()], network
                else:
                    return 1, [], network

    def RemoveBond(self, network, addr):
        """
        Removes a bond from the internal memory of the coordinator device. Device is also removed from discovered nodes.
        Does not affect configuration of the node device.
        :param network: Dictionary representing the emulated network.
        :param addr: Address of node device to be removed.
        :return: Error number, response data and network.
        """
        if 0 <= addr < 240:
            if self.bonded[addr] == 1:
                self.bonded[addr] = 0
                self.discovered[addr] = 0
                return 0, [self.countBonded()], network
            else:
                return 1, [], network
        else:
            return 1, [], network

    def Discovery(self, network, maxaddr):
        """
        Runs the discovery process. If a node is bonded in the internal memory of coordinator, it is added to list of discovered nodes.
        :param network: Dictionary representing the emulated network.
        :param maxaddr: Maximum address to be used for discovery. If the address is 0, up to 239 node devices are discovered.
        :return: Error number, number of discovered nodes and network.
        """
        if maxaddr == 0:
            maxaddr = 239
        count = 0
        for i in range(1, maxaddr):
            if str(i) in network:
                if self.bonded[i] == 1:
                    self.discovered[i] = 1
                    count += 1
        return 0, [count], network

    def SetDpa(self, network, dpaparam):
        """
        This functions sets new value of DPA param and returns previous value.
        :param network: dictionary, represent the emulated network
        :param dpaparam: int, new value of DPA param
        """
        pdpaparam = network['0'].dpaval
        network['0'].dpaval = dpaparam
        return 0, [pdpaparam], network

    def SetHops(self, network, rqhops, rsphops):
        """
        This function sets new value of request hops and response hops and than returns previous values.
        :param network: dictionary, represent the emulated network
        :param rqhops: int, new value of request hops
        :param rsphops: int, new value of response hops
        """
        prq = self.rqhops
        prsp = self.rsphops
        self.rqhops = rqhops
        self.rsphops = rsphops
        return 0, [prq, prsp], network

    def Backup(self, network, index):
        """
        The function reads network information data.
        :param network: dictionary, represent the emulated network
        :param index: int, index in the data
        """
        ret = []
        if index > 49:
            return 6, ret, network
        else:
            for i in range(index, len(self.network_data)):
                ret.append(self.network_data[i])
        return 0, ret, network

    def Restore(self, network, data):
        """
        The function writes new network data to coordinator.
        :param network: dictionary, represent the emulated network
        :param data: list, data that will be written
        """
        if len(data) != 49:
            return 6, [], network
        self.network_data = []
        for i in range(0, len(data)):
            self.network_data.append(data[i])
        return 0, [], network

    def SmartConnect(self, network, addr, bonding_test, ibk, mid, res0, virtual_address, res1, data):
        """
        This function sets on each device bonded value to 1. These values are overwritten in the internal memory of coordinator.
        :param network: dictionary, represent the emulated network
        :param addr: int, address of device in the network
        :param bonding_test:int, maximum number of FRCs
        :param ibk: list, value of individual bonding key
        :param mid: this variable represent mid value of device
        :param res0: list, reserved
        :param virtual_address: int, virtual device address
        :param res1: list, reserved, it is fill with zeros
        :param data: list, data reserved for future optional data
        """
        if addr == 0:
            for i in range(1, 240):
                if self.bonded[i] == 0:
                    self.bonded[i] = 1
                    self.SetMID(network, mid, addr)
                    network[str(addr)].embedpers[str(1)].ibk = ibk
                    return 0, [i, self.countBonded()], network
            return 1, [], network
        elif addr >= 240 or addr < 0:
            return 1, [], network
        else:
            if self.bonded[addr] == 1:
                return 1, [], network
            else:
                self.bonded[addr] = 1
                self.SetMID(network, mid, addr)
                network[str(addr)].embedpers[str(1)].ibk = ibk
                return 0, [addr, self.countBonded()], network

    def SetMID(self, network, mid, addr):
        """
        This function sets the Mid value of node in the coordinator's database.
        :param network: dictionary, represent the emulated network
        :param mid: this variable represent mid value of device
        :param addr: int, address of device in the network
        """
        self.mids[str(addr)] = mid[::-1]
        return 0, [], network

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, network, cmd, data):
        """
        Request handler for the coordinator peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param network: Dictionary representing the emulated network.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            return self.AddrInfo(network)
        elif cmd == 1:
            return self.DiscoveredNodes(network)
        elif cmd == 2:
            return self.BondedNodes(network)
        elif cmd == 3:
            return self.ClearBonds(network)
        elif cmd == 4:
            return self.BondNode(network, data[0])
        elif cmd == 5:
            return self.RemoveBond(network, data[0])
        elif cmd == 7:
            return self.Discovery(network, data[1])
        elif cmd == 8:
            return self.SetDpa(network, data[0])
        elif cmd == 9:
            return self.SetHops(network, data[0], data[1])
        elif cmd == 11:
            return self.Backup(network, data[0])
        elif cmd == 12:
            return self.Restore(network, data[0:len(data)])
        elif cmd == 18:
            return self.SmartConnect(network, data[0], data[1], data[2:18], data[18:22], data[22:24], data[24],
                                     data[25:34], data[34:len(data)])
        elif cmd == 19:
            return self.SetMID(network, data[0:4], data[4])
        elif cmd == 63:
            return 0, self.GetPerInfo(), network
        else:
            return 2, [], network
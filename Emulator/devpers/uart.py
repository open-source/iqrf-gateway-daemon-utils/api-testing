# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Uart:
    """
    Embed UART peripheral class.
    """
    def __init__(self):
        self.pert = 12
        self.perte = 3
        self.par1 = 55
        self.par2 = 0
        self.transbuffer = [-1] * 64
        self.recbuffer = [-1] * 64
        self.baudrate = -1

    def OpenUart(self, baudrate):
        """
        Opens the UART interface with specified transmission speed.
        :param baudrate: Transmission speed to open the interface with.
        :return: Error number and empty response data.
        """
        if 0 <= baudrate <= 8:
            self.baudrate = baudrate
            return 0, []
        else:
            return 6, []

    def CloseUart(self):
        """
        Closes the UART interface.
        :return: Error number and empty response data.
        """
        self.baudrate = -1
        return 0, []

    def WriteRead(self, timeout, data):
        """
        Writes and/or reads to/from receiving and transmitting buffers. If timeout is 255, no data should be read.
        :param timeout: Time to wait wait before reading after incoming data is written to the buffer.
        :param data: Data to write to the receiving buffer.
        :return: Error number and response data.
        """
        if self.baudrate == -1:
            return 1, []
        else:
            if timeout == 255:
                for i in range(len(data)):
                    self.recbuffer[i] = data[i]
                self.transbuffer = self.recbuffer.copy()
                return 0, []
            else:
                ret = []
                for i in range(len(data)):
                    self.recbuffer[i] = data[i]
                for i in range(len(self.transbuffer)):
                    if self.transbuffer[i] != -1:
                        ret.append(self.transbuffer[i])
                self.transbuffer = self.recbuffer.copy()
                return 0, ret

    def ClearWriteRead(self, timeout, data):
        """
        Writes and/or reads to/from receiving and transmitting buffers after the receiving buffer is cleared. Otherwise works the same as WriteRead.
        :param timeout: Time to wait wait before reading after incoming data is written to the buffer.
        :param data: Data to write to the receiving buffer.
        :return: Error number and response data.
        """
        self.recbuffer = [-1] * 64
        return self.WriteRead(timeout, data)

    def GetPerInfo(self):
        """
        Returns an array containing peripheral specific properties for peripheral information request.
        """
        return [self.perte, self.pert, self.par1, self.par2]

    def HandleRequest(self, cmd, data):
        """
        Request handler for the UART peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :param data: DPA request data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            return self.OpenUart(data[0])
        elif cmd == 1:
            return self.CloseUart()
        elif cmd == 2:
            return self.WriteRead(data[0], data[1:len(data)])
        elif cmd == 3:
            return self.ClearWriteRead(data[0], data[1:len(data)])
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
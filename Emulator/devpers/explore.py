# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

def Enumerate(network, addr):
    """
    Creates peripheral enumeration array from a device specified by address.
    :param network: Dictionary representing the emulated network.
    :param addr: Address of the node to enumerate peripherals from.
    :return: Error number and response data.
    """
    if str(addr) in network:
        epers = [0] * 32
        pers = network[str(addr)].embedpers
        hwpid = network[str(addr)].hwpid
        dpaver = pers['2'].dpaver

        for key in pers:
            epers[int(key)] = 1

        retpers = []
        for i in range(4):
            tmp = epers[i*8:(i*8+8)][::-1]
            tmp = ''.join(str(j) for j in tmp)
            retpers.append(int(tmp, 2))

        ret = [dpaver, len(network[str(addr)].stdpers)]
        ret.extend(retpers)
        ret.extend([hwpid & 0xff, (hwpid >> 8) & 0xff])
        ret.append(b'\x00\x00\x05')

        pers = network[str(addr)].stdpers
        if len(pers) != 0:
            stdpers = []
            for key in pers:
                stdpers.append(int(key))
            maxpnum = ((max(stdpers) - 32) + 7) & (-8)
            userper = [0] * maxpnum
            for i in stdpers:
                userper[i - 32] = 1
            for i in range(int(maxpnum/8)):
                tmp = userper[i*8:(i*8+8)][::-1]
                tmp = ''.join(str(j) for j in tmp)
                ret.append(int(tmp, 2))

        return 0, ret

    return 1, []

def GetMorePerInfo(network, addr, first):
    """
    Collects peripheral information of multiple peripherals from a single device.
    :param network: Dictionary representing the emulated network.
    :param addr: Target device address.
    :param first: Number of the first peripheral to read information from.
    :return: Error number and response data.
    """
    if str(addr) in network:
        dev = network[str(addr)]
        ret = []
        for i in range(first, 14):
            if str(i) in dev.embedpers:
                ret.append(dev.embedpers[str(i)].perte)
                ret.append(dev.embedpers[str(i)].pert)
                ret.append(dev.embedpers[str(i)].par1)
                ret.append(dev.embedpers[str(i)].par2)
            else:
                ret.extend([3,0,0,0])
        while ret[len(ret)-4:len(ret)] == [3, 0, 0, 0]:
            ret = ret[0:len(ret)-4]
        return 0, ret
    else:
        return 1, []

def HandleRequest(network, addr, cmd):
    """
    Request handler for the device exploration feature. If the given command number is a valid or supported command,
    the respective function handling this command is called and then the result is returned.
    :param network: Dictionary representing the emulated network.
    :param addr: Address specifying a device.
    :param cmd: Command number.
    :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
    """
    if 0 <= cmd <= 14:
        return GetMorePerInfo(network, addr, cmd)
    elif cmd == 63:
        return Enumerate(network, addr)
    else:
        return 2, []


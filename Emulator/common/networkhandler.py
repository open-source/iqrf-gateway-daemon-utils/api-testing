# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import struct
import time
from common.iqrfEnums import IqrfPnum
from devpers import device, ledr, ledg, coordinator, node, os, binaryoutput, thermometer, \
    explore, spi, eeprom, uart, ram, eeeprom, light, io, sensor, frc


def buildNetwork(data):
    """
    Creates a dictionary of device objects with peripherals representing the emulated network. Device address is used as key.
    :param data: A dictionary containing configuration from JSON file, of the emulated network to be built.
    :return network: Dictionary containing the built network.
    """
    network = dict()
    nadr_array = []
    for item in data:
        dev = data[item]
        network[str(dev['nadr'])] = device.Device(dev['hwpid'], dev['dpaval'], dev['mid'], [], dev['configuration'], dev['rfpgm'], dev['undocumented'])
        # each device is either a coordinator or a node device
        # coordinator device always has the FRC peripheral enabled
        # each device always has the OS peripheral enabled
        if dev['nadr'] == IqrfPnum.Coordinator:
            network[str(dev['nadr'])].embedpers['0'] = coordinator.Coordinator(dev['did'])
            network["0"].embedpers["13"] = frc.Frc()
        else:
            network[str(dev['nadr'])].embedpers['1'] = node.Node(dev['bonded'])
            network["0"].embedpers["0"].bonded[dev['nadr']] = dev['bonded']
        ibk_array = []
        for ibk in dev['ibk']:
            ibk_array.append(ibk)
        network[str(dev['nadr'])].ibk = ibk_array
        network[str(dev['nadr'])].embedpers['2'] = os.Os()

        nadr_array.append(dev['nadr'])
        if len(nadr_array) != len(set(nadr_array)):
            print("nadr value is not unique")
            exit(1)

        # optional peripherals
        for per in dev['per']:
            if per['pnum'] == IqrfPnum.EEPROM:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = eeprom.Eeprom()
            elif per['pnum'] == IqrfPnum.EEEPROM:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = eeeprom.Eeeprom()
            elif per['pnum'] == IqrfPnum.RAM:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = ram.Ram()
            elif per['pnum'] == IqrfPnum.LEDR:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = ledr.Ledr()
            elif per['pnum'] == IqrfPnum.LEDG:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = ledg.Ledg()
            elif per['pnum'] == IqrfPnum.SPI:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = spi.Spi()
            elif per['pnum'] == IqrfPnum.IO:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = io.Io(per['data'])
            elif per['pnum'] == IqrfPnum.Thermometer:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = thermometer.Thermometer(per['temp'])
            elif per['pnum'] == IqrfPnum.UART:
                network[str(dev['nadr'])].embedpers[str(per['pnum'])] = uart.Uart()
            elif per['pnum'] == IqrfPnum.BO:
                network[str(dev['nadr'])].stdpers[str(per['pnum'])] = binaryoutput.BinaryOutput(per['outputs'])
            elif per['pnum'] == IqrfPnum.Sensor:
                network[str(dev['nadr'])].stdpers[str(per['pnum'])] = sensor.Sensor()
                i = 0
                for s in per['sensors']:
                    network[str(dev['nadr'])].stdpers[str(per['pnum'])].sensors[i] = sensor.ConvertSData(s['type'], s['value'])
                    i += 1
            elif per['pnum'] == IqrfPnum.Light:
                network[str(dev['nadr'])].stdpers[str(per['pnum'])] = light.Light()
    # nodes are automatically discovered
    _, _, network = network['0'].embedpers['0'].HandleRequest(network, 7, [0, 0])
    return network

def sendConfig(conneciton, network):
    """
    This function sends configuration of the coordinator node to Daemon client.
    :param conneciton: A connection session between server and client.
    :param network: Dictionary representing the emulated network.
    """
    time.sleep(0.1)
    penum = explore.HandleRequest(network, 0, 63)[1]
    penumb = b''
    # conversion of integers to hexadecimal byte string
    for i in range(len(penum)):
        if type(penum[i]) == int:
            penumb += struct.pack("B", penum[i])
        else:
            penumb += penum[i]
    configstring = b'\x00\x00\xff\x3f\x00\x00\x80\x00'+penumb
    conneciton.send(configstring)

def hexString(request):
    """
    Converts incoming DPA request as an array of hexadecimal numbers.
    :param request: DPA request in bytes.
    """
    requestHex = request.hex()
    return '.'.join(requestHex[i:i+2] for i in range(0, len(requestHex), 2))

def parseRequest(request):
    """
    Splits incoming DPA request into separate variables as integers.
    :param request: DPA request in bytes
    :return: returns address, number of peripheral, command, hwpid and optional data as separate variables
    """
    nadr = request[0]
    pnum = request[2]
    pcmd = request[3]
    hwpid = [request[4], request[5]]
    if (len(request) > 6):
        pdata = request[6:len(request)]
    else:
        pdata = []
    return nadr, pnum, pcmd, hwpid, pdata

def buildResponse(nadr, pnum, pcmd, hwpid, errn, dpaval, pdata):
    """
    Converts individual parts of a response to bytes and then creates a DPA response string.
    :param nadr: Address of device.
    :param pnum: Peripheral number.
    :param pcmd: Peripheral command + 128 to signify response.
    :param hwpid: Device hwpid.
    :param errn: Dpa error number.
    :param dpaval: Device dpa value.
    :param pdata: Optional data of the response.
    :return: DPA response in bytes.
    """
    data = b''
    nadr = struct.pack("B", nadr) + struct.pack("B", 0)
    pnum = struct.pack("B", pnum)
    pcmd = struct.pack("B", pcmd)
    hwpid = struct.pack("B", hwpid[0]) + struct.pack("B", hwpid[1])
    errn = struct.pack("B", errn)
    dpaval = struct.pack("B", dpaval)
    for i in range(0, len(pdata), 1):
        if type(pdata[i]) == bytes:
            data += pdata[i]
        else:
            data += struct.pack("B", pdata[i])
    return nadr+pnum+pcmd+hwpid+errn+dpaval+data

def executeBatch(network, address, pnum, pcmd, hwpid, pdata):
    """
    Executes a command from the batch request.
    :param network: Dictionary representing the emulated network.
    :param address: Address of the target device.
    :param pnum: Peripheral number.
    :param pcmd: Peripheral command.
    :param hwpid: Device hwpid.
    :param pdata: Optional data of the command.
    """
    if address == 0 or (str(address) in network and network[str(address)].embedpers[str(1)].bonded == 1):
        if str(pnum) in network[str(address)].embedpers:
            peripheral = network[str(address)].embedpers[str(pnum)]
            if pnum == IqrfPnum.Coordinator:
                # cannot be a part of the batch request
                if pcmd in [4, 7, 13, 18]:
                    return
                _, _, network = peripheral.HandleRequest(network, pcmd, pdata)
            elif pnum == IqrfPnum.Node:
                _, _, network = peripheral.HandleRequest(network, address, pcmd, pdata)
            elif pnum == IqrfPnum.OS:
                # cannot be a part of the batch request
                if pcmd in [5, 11]:
                    return
                else:
                    _, _, network = peripheral.HandleRequest(network, address, pcmd, pdata)
            elif pnum == IqrfPnum.EEPROM:
                peripheral.HandleRequest(address, pcmd, pdata)
            elif pnum == IqrfPnum.EEEPROM:
                peripheral.HandleRequest(pcmd, pdata)
            elif pnum == IqrfPnum.RAM:
                peripheral.HandleRequest(pcmd, pdata)
            elif pnum == IqrfPnum.LEDR:
                peripheral.HandleRequest(pcmd)
            elif pnum == IqrfPnum.LEDG:
                peripheral.HandleRequest(pcmd)
            elif pnum == IqrfPnum.SPI:
                peripheral.HandleRequest(pcmd, pdata)
            elif pnum == IqrfPnum.IO:
                peripheral.HandleRequest(pcmd, pdata)
            elif pnum == IqrfPnum.Thermometer:
                peripheral.HandleRequest(pcmd)
            elif pnum == IqrfPnum.UART:
                peripheral.HandleRequest(pcmd, pdata)
        elif str(pnum) in network[str(address)].stdpers:
            peripheral = network[str(address)].stdpers[str(pnum)]
            if pnum == IqrfPnum.BO:
                peripheral.HandleRequest(pcmd, pdata)
            elif pnum == IqrfPnum.Sensor:
                peripheral.HandleRequest(pcmd, pdata)
            elif pnum == IqrfPnum.Light:
                peripheral.HandleRequest(pcmd, pdata)
        elif pnum == IqrfPnum.Explore:
            explore.HandleRequest(network, address, pcmd)

def processBatch(network, selective, address, data):
    """
    Parses the batch request, splits the request data into individual commands to be executed on selected nodes or a single node.
    :param network: Dictionary representing the emulated network.
    :param selective: Variable specifying if the batch request is selective or not.
    :param address: Target device address.
    :param data: List of commands to be executed.
    """
    if not data:
        return 5, [], network
    if selective:
        if len(data) <= 30:
            return 5, [], network
        nodes = []
        # convert bitmask list to a list of integers representing affected node addresses
        for i in range(30):
            tmp = format(data[i], '08b')
            tmp = tmp[::-1]
            for j in range(8):
                if int(tmp[j]) == 1:
                    count += 1
                nodes.append(int(tmp[j]))
        for i in range(len(nodes)):
            if nodes[i] == 1:
                for j in range(30, len(data)):
                    length = data[j]
                    if length == 0:
                        break
                    if (j+length) > length(data):
                        break
                    pnum = data[j+1]
                    pcmd = data[j+2]
                    hwpid = data[j+3:j+5]
                    pdata = []
                    if length > 5:
                        pdata = data[j+5:j+length]
                    executeBatch(network, i, pnum, pcmd, hwpid, pdata)
                    j += length
        return 0, [], network
    else:
        for i in range(len(data)):
            length = data[i]
            if length == 0:
                break
            if (i+length) > len(data):
                break
            pnum = data[i+1]
            pcmd = data[i+2]
            hwpid = data[i+3:i+5]
            pdata = []
            if length > 5:
                pdata = data[i+5:i+length]
            executeBatch(network, address, pnum, pcmd, hwpid, pdata)
            i += length
        return 0, [], network

def processRequest(network, request):
    """
    Calls a function to parse request. Parsed data are used to determine the target device and peripheral.
    If the target device exists and implements specified peripheral, the request data are given to the peripheral to execute command.
    Once the command execution is finished, the returned data from peripheral is sent to be converted to bytes and then returned as DPA response.
    :param network: Dictionary representing the emulated network.
    :param request: DPA request in bytes.
    :return response: DPA response in bytes.
    """
    rqNadr, rqPnum, rqCmd, rqHwpid, rqPdata = parseRequest(request)
    errn = 0
    dpval = 0
    min, max = 0, 239
    if rqNadr <= 239:
        min, max = rqNadr, rqNadr + 1
    for addr in range(min, max):
        if str(addr) in network:
            rqHwpid = [0,0]
            if addr != 0 and network[str(addr)].embedpers[str(1)].bonded != 1 and network['0'].embedpers['0'].bonded[addr] != 1:
                errn, rqPdata = 8, []
                rqHwpid = [255, 255]
            elif addr != 0 and network[str(addr)].embedpers[str(1)].bonded != 1 and network['0'].embedpers['0'].bonded[addr] == 1:
                return b''
            elif str(rqPnum) in network[str(addr)].embedpers:
                peripheral = network[str(addr)].embedpers[str(rqPnum)]
                if rqPnum == IqrfPnum.Coordinator:
                    errn, rqPdata, network = peripheral.HandleRequest(network, rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.Node:
                    errn, rqPdata, network = peripheral.HandleRequest(network, addr, rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.OS:
                    if rqCmd == 5:
                        errn, rqPdata, network = processBatch(network, False, addr, rqPdata)
                    elif rqCmd == 11:
                        if addr == 0:
                            errn, rqPdata = 3, []
                        else:
                            errn, rqPdata, network = processBatch(network, True, addr, rqPdata)
                    else:
                        errn, rqPdata, network = peripheral.HandleRequest(network, addr, rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.EEPROM:
                    errn, rqPdata = peripheral.HandleRequest(addr, rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.EEEPROM:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.RAM:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.LEDR:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd)
                elif rqPnum == IqrfPnum.LEDG:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd)
                elif rqPnum == IqrfPnum.SPI:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.IO:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.Thermometer:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd)
                elif rqPnum == IqrfPnum.UART:
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.FRC:
                    errn, rqPdata = peripheral.HandleRequest(network, rqCmd, rqPdata)
            elif str(rqPnum) in network[str(addr)].stdpers:
                peripheral = network[str(addr)].stdpers[str(rqPnum)]
                if rqPnum == IqrfPnum.BO:
                    rqHwpid = [network[str(addr)].hwpid & 0xff, (network[str(addr)].hwpid >> 8) & 0xff]
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.Sensor:
                    # This line of code inspired by a StackOverflow thread, see comment above.
                    rqHwpid = [network[str(addr)].hwpid & 0xff, (network[str(addr)].hwpid >> 8) & 0xff]
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
                elif rqPnum == IqrfPnum.Light:
                    # This line of code inspired by a StackOverflow thread, see comment above.
                    rqHwpid = [network[str(addr)].hwpid & 0xff, (network[str(addr)].hwpid >> 8) & 0xff]
                    errn, rqPdata = peripheral.HandleRequest(rqCmd, rqPdata)
            elif rqPnum == IqrfPnum.Explore:
                errn, rqPdata = explore.HandleRequest(network, addr, rqCmd)
            else:
                if rqPnum == IqrfPnum.EEEPROM:
                    errn, rqPdata = 1, []
                elif rqPnum == IqrfPnum.Thermometer:
                    errn, rqPdata = 0, [128, 0, 0]
                elif rqPnum == IqrfPnum.SPI:
                    errn, rqPdata = 3, []
                else:
                    errn, rqPdata = 3, []
            dpval = network[str(addr)].dpaval
        else:
            errn, rqPdata = 8, []
    if rqNadr == 255:
        return b''
    response = buildResponse(rqNadr, rqPnum, rqCmd + 128, rqHwpid, errn, dpval, rqPdata)
    return response

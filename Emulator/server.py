# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import argparse, json, pathlib, socket, sys
from jsonschema.validators import Draft7Validator
from common import networkhandler as nh

address = '0.0.0.0'
portnum = 10000
BUFFER_SIZE = 1024
sockfd = None

parser = argparse.ArgumentParser()
parser.add_argument('-c', type=str, required=True, help='path to JSON file containing network configuration')
args = parser.parse_args()

with open(args.c) as file:
    data = json.load(file)
file.close()

schema_path = pathlib.Path(__file__).parent / 'common/validate.json'
with schema_path.open() as f:
    schema_data = f.read()
f.close()

schema = json.loads(schema_data)
validator = Draft7Validator(schema)
schema_errors = sorted(validator.iter_errors(data), key=str)

if schema_errors:
    for error in schema_errors:
        print(error)
    exit(2)

network = nh.buildNetwork(data)

try:
    sockfd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error as msg:
    print(msg, file=sys.stderr)
    sockfd.close()
    exit(3)

try:
    sockfd.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
except socket.error as msg:
    print(msg, file=sys.stderr)
    sockfd.close()
    exit(3)

try:
    sockfd.bind((address, portnum))
except socket.error as msg:
    print(msg, file=sys.stderr)
    sockfd.close()
    exit(3)

try:
    sockfd.listen(1)
except socket.error as msg:
    print(msg, file=sys.stderr)
    sockfd.close()
    exit(3)

while True:
    print("Awaiting client connection.")
    connection, addr = sockfd.accept()
    print("Client connected: ", addr)
    nh.sendConfig(connection, network)
    while True:
        request = connection.recv(BUFFER_SIZE)
        if not request:
            break
        print("Incoming request: " + nh.hexString(request))
        if request == b'\x00\x00\xfd\x00\xff\xff':
            network = nh.buildNetwork(data)
            continue
        response = nh.processRequest(network, request)
        if response == b'':
            continue
        print("Outgoing response: " + nh.hexString(response))
        connection.send(response)
    connection.close()
    print("Closing client connection.")
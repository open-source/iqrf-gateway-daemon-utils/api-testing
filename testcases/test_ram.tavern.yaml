# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

test_name: ram_test

includes:
  - !include includes.yaml

paho-mqtt:
  client:
    transport: tcp
    client_id: "{client_id:s}"
  connect:
    host: "{host_emulator:s}"
    port: !int "{port:d}"
    timeout: 5
    keepalive: 30

stages:
  - name: ram_write
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/WriteRAM_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/WriteRAM_response.yaml
      timeout: 20

  - name: ram_read
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/ReadRAM_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/ReadRAM_response.yaml
      timeout: 20

  - name: ram_perinfo
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/ram_perinfo_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/ram_perinfo_response.yaml
      timeout: 20

  - name: ram_write_invalid
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/ram_write_invalid_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/ram_write_invalid_response.yaml
      timeout: 20

  - name: ram_read_invalid
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/ram_read_invalid_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/ram_read_invalid_response.yaml
      timeout: 20

  - name: ram_perinfo_invalid
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/ram_perinfo_invalid_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/ram_perinfo_invalid_response.yaml
      timeout: 20

  - name: ram_write2
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/WriteRAM2_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/WriteRAM2_response.yaml
      timeout: 20

  - name: ram_read2
    mqtt_publish:
      topic: Iqrf/DpaRequest
      json: !include ram/ReadRAM2_request.yaml

    mqtt_response:
      topic: Iqrf/DpaResponse
      json: !include ram/ReadRAM2_response.yaml
      timeout: 20



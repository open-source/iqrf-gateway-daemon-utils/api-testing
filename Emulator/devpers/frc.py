# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Frc:
    """
    Embed FRC peripheral class.
    """
    def __init__(self):
        self.pert = 14
        self.perte = 3
        self.par1 = 55
        self.par2 = 0
        self.frcparam = 0
        self.extradata = [0] * 8

    def Send(self, network, frccmd, userdata):
        """
        Collects data from devices in the network. Type of collected data is specified by the FRC command and user data.
        :param network: Dictionary representing the emulated network.
        :param frccmd: FRC command number.
        :param userdata: Array of user data for FRC commands.
        :return: Error number and response data.
        """
        ret = []
        ncount = 0
        responding = 0
        if 0 <= frccmd <= 127: #2bits
            ncount = 512
            collected = [0]
            if frccmd == 0: # ping
                for i in range(1, ncount):
                    if str(i) in network and network[str(i)].embedpers['1'].bonded == 1:
                        collected.append(1)
                        responding += 1
                    else:
                        collected.append(0)
                for i in range(64):
                    tmp = collected[i*8:(i*8+8)][::-1]
                    tmp = ''.join(str(j) for j in tmp)
                    ret.append(int(tmp, 2))

            if responding == 0:
                ret = [255]+ret
            else:
                ret = [responding]+ret
            self.extradata = ret[56:len(ret)]
            ret = ret[0:56]
            return 0, ret
        elif 128 <= frccmd <= 223: #1byte
            ncount = 64
            if frccmd == 128: # temperature
                for i in range(1, ncount):
                    if str(i) in network and str(10) in network[str(i)].embedpers and network[str(i)].embedpers['1'].bonded == 1:
                        temp = network[str(i)].embedpers['10'].Read()[1][0]
                        if temp == 0:
                            temp = 127
                        ret.append(temp)
                        responding += 1
                    else:
                        ret.append(0)

            if responding == 0:
                ret = [255]+ret
            else:
                ret = [responding]+ret
            self.extradata = ret[56:len(ret)]
            ret = ret[0:56]
            return 0, ret
        elif 224 <= frccmd <= 247: #2byte
            return 1, []
        else: #4byte
            ncount = 16
            return 1, []

    def ExtraResult(self):
        """
        Sends remaining data from FRC request that did not fit in the original DPA response.
        :return: Error number and remained of the FRC data.
        """
        return 0, self.extradata

    def SetParams(self, frctime):
        """
        Sets the FRC parameter of the peripheral. If the requested FRC parameter value is between the allowed values, the lower value is always used.
        :param frctime: Value of the FRC parameter to set.
        :return: Error number and empty response data.
        """
        ret = [self.frcparam]
        if frctime >= 128:
            frctime -= 128
        if 0 <= frctime <= 15:
            self.frcparam = 0
        elif 16 <= frctime <= 31:
            self.frcparam = 16
        elif 32 <= frctime <= 47:
            self.frcparam = 32
        elif 48 <= frctime <= 63:
            self.frcparam = 48
        elif 64 <= frctime <= 79:
            self.frcparam = 64
        elif 80 <= frctime <= 95:
            self.frcparam = 80
        elif 96 <= frctime <= 111:
            self.frcparam = 96
        elif 112 <= frctime <= 127:
            self.frcparam = 112
        return 0, ret

    def GetPerInfo(self):
        """
        Returns an array containing peripheral specific properties for peripheral information request.
        """
        return [self.perte, self.pert, self.par1, self.par2]

    def HandleRequest(self, network, cmd, data):
        """
        Request handler for the FRC peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param network: Dictionary representing the emulated network.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            if 3 <= len(data) <= 31:
                return self.Send(network, data[0], data[1:len(data)])
        elif cmd == 1:
            if len(data) != 0:
                return 5, []
            else:
                return self.ExtraResult()
        elif cmd == 2:
            return 3, []
        elif cmd == 3:
            if len(data) != 1:
                return 5, []
            else:
                return self.SetParams(data[0])
        if cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
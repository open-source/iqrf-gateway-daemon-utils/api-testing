# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Spi:
    def __init__(self):
        self.pert = 8
        self.perte = 3
        self.par1 = 55
        self.par2 = 0
        self.rx = [-1] * 55
        self.tx = [-1] * 55

    def WriteRead(self, timeout, data):
        """
        The function writes and reads data
        :param timeout: int, if timeout is 255 that no data be read
        :param data: list, data that will be written
        """
        if timeout == 255:
            j=0
            for i in data:
                self.rx[j] = data[j]
                j+=1
            self.tx = self.rx.copy()
            return 0, []
        else:
            ret = []
            j=0
            for i in data:
                self.rx[j] = data[j]
                j+=1
            for i in self.tx:
                if i != -1:
                    ret.append(i)
            self.tx = self.rx.copy()
            return 0, ret

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return ret

    def HandleRequest(self, cmd, data):
        """
        Handler for the eeprom peripheral.
        :param cmd: list, optional command data
        :param data: list, optional command data
        """
        if cmd == 0:
            return self.WriteRead(data[0], data[1:len(data)])
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
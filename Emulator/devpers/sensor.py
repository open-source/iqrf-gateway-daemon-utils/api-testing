# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from common.iqrfEnums import IqrfSType

def ConvertSData(type, value):
    """
    Multiplies sensor values read from the configuration file by the inverse of their resolution. Each sensor type has a specific unit resolution.
    This function is used when the emulated network is built.
    :param type: Sensor type.
    :param value: Value read from the sensor before resolution is accounted for.
    :return:
    """
    if type == IqrfSType.Temp:
        return [type, round(value*16)]
    elif type == IqrfSType.CO2:
        return [type, round(value)]
    elif type == IqrfSType.VOC:
        return [type, round(value)]
    elif type == IqrfSType.ELVoltage:
        return [type, round(value*1000)]
    elif type == IqrfSType.EMG:
        return [type, round(value*10000)]
    elif type == IqrfSType.LVoltage:
        return [type, round(value*16)]
    elif type == IqrfSType.Current:
        return [type, round(value*1000)]
    elif type == IqrfSType.Power:
        return [type, round(value*4)]
    elif type == IqrfSType.MFreq:
        return [type, round(value*1000)]
    elif type == IqrfSType.TimeSpan:
        return [type, round(value)]
    elif type == IqrfSType.Lux:
        return [type, round(value)]
    elif type == IqrfSType.NO2:
        return [type, round(value*1000)]
    elif type == IqrfSType.SO2:
        return [type, round(value*1000)]
    elif type == IqrfSType.CO:
        return [type, round(value*100)]
    elif type == IqrfSType.O3:
        return [type, round(value*10000)]
    elif type == IqrfSType.APressure:
        return [type, round(value*16)]
    elif type == IqrfSType.CTemp:
        return [type, round(value)]
    elif type == IqrfSType.PPM25:
        return [type, round(value*4)]
    elif type == IqrfSType.SPL:
        return [type, round(value*16)]
    elif type == IqrfSType.Alt:
        return [type, round((value + 1024)*4)]
    elif type == IqrfSType.Acc:
        return [type, round(value*256)]
    elif type == IqrfSType.NH3:
        return [type, round(value*10)]
    elif type == IqrfSType.Methane:
        return [type, round(value*1000)]
    elif type == IqrfSType.ShortL:
        return [type, round(value*1000)]
    elif type == IqrfSType.PPM1:
        return [type, round(value*4)]
    elif type == IqrfSType.PPM4:
        return [type, round(value*4)]
    elif type == IqrfSType.PPM10:
        return [type, round(value*4)]
    elif type == IqrfSType.RHumidity:
        return [type, round(value*2)]
    elif type == IqrfSType.Bin7:
        return [type, value]
    elif type == IqrfSType.PowerFactor:
        return [type, round(value*200)]
    elif type == IqrfSType.UVIndex:
        return [type, round(value*8)]
    elif type == IqrfSType.pH:
        return [type, round(value*16)]
    elif type == IqrfSType.Bin30:
        return [type, value]
    elif type == IqrfSType.Consumption:
        return [type, value]
    elif type == IqrfSType.Datetime:
        return [type, value]
    elif type == IqrfSType.TimeSpanLong:
        return [type, round(value*16)]
    elif type == IqrfSType.Lat:
        return [type, value]
    elif type == IqrfSType.Lon:
        return [type, value]
    elif type == IqrfSType.Data:
        ret = [type]
        for i in value:
            ret.append(i)
        return ret

class Sensor:
    """
    Standard Sensor peripheral class.
    """
    def __init__(self):
        self.sensors = dict()

    def ReadS(self, i):
        """
        Reads data from a sensor specified by index.
        :param i: Index of a sensor.
        :return: Value read from the sensor.
        """
        if IqrfSType.Temp <= self.sensors[i][0] <= IqrfSType.PPM10:
            return [self.sensors[i][1] & 0xff, (self.sensors[i][1] >> 8) & 0xff]
        elif IqrfSType.RHumidity <= self.sensors[i][0] <= IqrfSType.pH:
            return [self.sensors[i][1]]
        elif IqrfSType.Bin30 <= self.sensors[i][0] <= IqrfSType.Lon:
            return [self.sensors[i][1] & 0xff, (self.sensors[i][1] >> 8) & 0xff, (self.sensors[i][1] >> 16) & 0xff, (self.sensors[i][1] >> 24) & 0xff]
        elif self.sensors[i][0] == IqrfSType.Data:
            return self.sensors[i][1:len(self.sensors[i])]

    def ReadSType(self, i):
        """
        Reads data from a sensor specified by index together with the sensor type. Calls the ReadS function.
        :param i: Index of a sensor.
        :return: Sensor type and value read from the sensor.
        """
        ret = [self.sensors[i][0]]
        ret.extend(self.ReadS(i))
        return ret

    def Read(self, types, sensors, data):
        """
        Handles the read DPA request. Checks the bitmask of sensor indexes to read data from and then calls a data collecting function.
        If the value of types is not 0, the values read from sensors are added to sensor type in response.
        :param types: Value specifying whether or not values from sensors should be read with their type.
        :param sensors: Bitmask of sensors to read values from.
        :param data: Optional request data.
        :return: Error number and empty response data, or data containing values and/or types of sensors.
        """
        ret = []
        sensorlist = []
        for i in sensors:
            tmp = format(i, '08b')
            tmp = tmp[::-1]
            for j in range(8):
                sensorlist.append(int(tmp[j]))

        if types == 0:
            for i in range(len(sensorlist)):
                if sensorlist[i] == 1 and i in self.sensors:
                    ret.extend(self.ReadS(i))
        else:
            for i in range(len(sensorlist)):
                if sensorlist[i] == 1 and i in self.sensors:
                    ret.extend(self.ReadSType(i))
        # if the data does not fit in a single DPA response, general error
        if len(ret) > 56:
            return 1, []
        return 0, ret

    def Enumerate(self):
        """
        Returns types of sensors implemented by the sensor peripheral.
        :return: Error number and response data with sensor types.
        """
        ret = []
        for i in range(len(self.sensors)):
            ret.append(self.sensors[i][0])
        return 0, ret

    def HandleRequest(self, cmd, data):
        """
        Request handler for the Sensor standard. If the given standard command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            if len(data) == 0:
                return 0, [self.ReadS(0)]
            elif len(data) == 4:
                return self.Read(0, data, [])
            else:
                if len(data) % 5 == 4:
                    return self.Read(0, data[0:4], data[4:len(data)])
                else:
                    return 5, []
        elif cmd == 1:
            if len(data) == 0:
                return 0, [self.ReadSType(0)]
            elif len(data) == 4:
                return self.Read(1, data, [])
            else:
                if len(data) % 5 == 4:
                    return self.Read(1, data[0:4], data[4:len(data)])
                else:
                    return 5, []
        elif cmd == 62:
            return self.Enumerate()
        else:
            return 2, []
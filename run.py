# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from tavern.core import run
import time

time.sleep(15)

result = False

result |= run('testcases/test_coordinator.tavern.yaml')
result |= run('testcases/test_os.tavern.yaml')
result |= run('testcases/test_eeprom.tavern.yaml')
result |= run('testcases/test_spi.tavern.yaml')
result |= run('testcases/test_ram.tavern.yaml')
result |= run('testcases/test_node.tavern.yaml')
result |= run('testcases/test_explore.tavern.yaml')
result |= run('testcases/test_ledg.tavern.yaml')
# result |= run('testcases/test_light.tavern.yaml')  # Light v0 was removed from the repository
run('testcases/reset_network.tavern.yaml')
result |= run('testcases/test_ledr.tavern.yaml')
result |= run('testcases/test_eeeprom.tavern.yaml')
result |= run('testcases/test_thermometer.tavern.yaml')
result |= run('testcases/test_io.tavern.yaml')
result |= run('testcases/test_uart.tavern.yaml')
result |= run('testcases/test_binaryoutput.tavern.yaml')
result |= run('testcases/test_sensor.tavern.yaml')
result |= run('testcases/test_frc.tavern.yaml')
result |= run('testcases/test_coordinator_discovery_bonding.tavern.yaml')
result |= run('testcases/test_os_control.tavern.yaml')

if result:
	exit(1)


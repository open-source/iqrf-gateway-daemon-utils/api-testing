# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Io:
    """
    Embed IO peripheral class.
    :param data: Data representing a device connected to the IO.
    """
    def __init__(self, data):
        self.direction = [0] * 5
        self.pullup = [0] * 5
        self.outputs = [0] * 5
        self.data = data
        self.pert = 9
        self.perte = 3
        self.par1 = 23
        self.par2 = 0

    def Direction(self, data):
        """
        Sets direction of IO port pins. Pins can either be set to output or input.
        :param data: Array containing triplets of port number, pin bitmask and value.
        :return: Error number and empty response data.
        """
        for i in range(0, len(data), 3):
            if (len(data) - i) < 3:
                break
            port, mask, value = data[i], data[i+1], data[i+2]
            if port in [0, 1, 2, 4]:
                self.direction[port] = 0xff & mask & value
                return 0, []
            elif port == 17:
                self.pullup[1] = 0xff & mask & value
                return 0, []
            else:
                return 6, []

    def Set(self, data):
        """
        Sets logic values of IO port pins.
        :param data: Array containing triplets of port number, pin bitmask and value.
        :return: Error number and empty response data.
        """
        for i in range(0, len(data), 3):
            if (len(data) - i) < 3:
                break
            cmd0, cmd1, cmd2 = data[i], data[i+1], data[i+2]
            if cmd0 in [0, 1, 2, 4]:
                self.outputs[cmd0] = ~self.direction[cmd0] & cmd1 & cmd2
            elif cmd0 == 255:
                # set delay, but response is sent immediately
                continue
            else:
                break
        return 0, []

    def Get(self):
        """
        Returns value read from a device connected to IO via port pins.
        """
        ret = []
        for i in range(len(self.direction)):
            if i == 3:
                ret.append(0)
                continue
            inputs = self.direction[i] & self.data[i]
            outputs = ~self.direction[i] & self.outputs[i]
            ret.append(inputs | outputs)
        return 0, ret

    def GetPerInfo(self):
        """
        Returns an array containing peripheral specific properties for peripheral information request.
        """
        return [self.perte, self.pert, self.par1, self.par2]

    def HandleRequest(self, cmd, data):
        """
        Request handler for the IO peripheral. If the given peripheral command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            return self.Direction(data)
        elif cmd == 1:
            return self.Set(data)
        elif cmd == 2:
            return self.Get()
        elif cmd == 63:
            return 0, self.GetPerInfo()
        else:
            return 2, []
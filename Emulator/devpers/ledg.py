# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class Ledg:
    def __init__(self):
        self.pert = 7
        self.perte = 3
        self.par1 = 1
        self.par2 = 0
        self.state = 0

    def Set(self, state):
        """
        The function set ledg on or off
        :param state: int, state of ledg
        """
        if state == 1:
            self.state = 1
        else:
            self.state = 0
        return 0, []

    def Pulse(self):
        return 0, []

    def Flash(self):
        self.state = 2
        return 0, []

    def GetPerInfo(self):
        ret = []
        ret.append(self.perte)
        ret.append(self.pert)
        ret.append(self.par1)
        ret.append(self.par2)
        return 0, ret

    def HandleRequest(self, cmd):
        """
        Handler for the ledg peripheral.
        :param cmd: int, address of device
        """
        if cmd == 0 or cmd == 1:
            return self.Set(cmd)
        elif cmd == 3:
            return self.Pulse()
        elif cmd == 4:
            return self.Flash()
        elif cmd == 63:
            return self.GetPerInfo()
        else:
            return 2, []
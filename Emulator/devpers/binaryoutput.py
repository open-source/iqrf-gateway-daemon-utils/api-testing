# The MIT License (MIT)
#
# Copyright (c) 2020-2023 Karel Hanák, Matej Mrázik
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

class BinaryOutput:
    """
    BinaryOutput standard peripheral class.
    :param n: Number of outputs implemented by the peripheral.
    """
    def __init__(self, n):
        self.implemented = n
        self.states = [0] * 32

    def SetOutputs(self, outputs, states):
        """
        Reads the states of outputs specified by a bitmask. If the request also contains additional data, outputs are set to a specified by this data.
        :param outputs: Bitmask specifying outputs to read or set state of.
        :param states: An array of states, each number in this array represents a state of an output.
        :return: Error number, empty response data if the request was invalid, response data with previous states of the outputs otherwise.
        """
        outs = []
        count = 0
        for i in outputs:
            tmp = format(i, '08b')
            tmp = tmp[::-1]
            for j in range(8):
                if int(tmp[j]) == 1:
                    count += 1
                outs.append(int(tmp[j]))

        # number of state values must equal to number of affected outputs
        if count != len(states):
            return 5, []

        pS = self.states.copy()
        prevStates = []
        for i in range(4):
            tmp = pS[i*8:(i*8+8)][::-1]
            tmp = ''.join(str(j) for j in tmp)
            prevStates.append(int(tmp, 2))

        k = 0
        for i in range(len(outs)):
            if i == self.implemented:
                break
            else:
                if outs[i] == 1:
                    if states[k] == 0:
                        self.states[i] = 0
                    elif states[k] == 1:
                        self.states[i] = 1
                    elif (2 <= states[k] <= 127) or (129 <= states[k] <= 255):
                        self.states[i] = 0
                    else:
                        return 1, []
                    k += 1
        return 0, prevStates

    def EnumerateOutputs(self):
        """
        Returns number of outputs implemented by the peripheral.
        :return: Error number and response data with count of binary outputs.
        """
        return 0, [self.implemented]

    def HandleRequest(self, cmd, data):
        """
        Request handler for the BinaryOutput standard. If the given standard command number is a valid or supported command,
        the respective function handling this command is called and then the result is returned.
        :param cmd: Peripheral command number.
        :param data: Optional command data.
        :return: Error number, empty response data and network if the command is not valid. Response from a respective function if valid.
        """
        if cmd == 0:
            if len(data) < 4:
                return 5, []
            else:
                return self.SetOutputs(data[0:4], data[4:len(data)])
        elif cmd == 62:
            return self.EnumerateOutputs()
        else:
            return 2, []
